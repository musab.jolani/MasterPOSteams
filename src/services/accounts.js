import axios from "axios";

class Accounts {
  async getAll(qry) {
    const article = `q=${qry}`;
    const headers = {
      Accept: `application/json, text/plain, */*`,
      Authorization:
        "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU5IiwidXNlcm5hbWUiOiJtdXNhYiIsImRpc3BsYXluYW1lIjoiTXVzYWIgSm9sYW5pIiwicm9vbXMiOlsiIl0sImVtYWlsIjoibXVzYWIuam9sYW5pQGdtYWlsLmNvbSIsIm51bWJlcnMiOlsiMCJdLCJwYXJ0bmVyIjoiMCJ9.bouA7T9-gWHm3yR77hN7NskcYH7M1Te6MNjzo6PiUGE",
    };
    return await (
      await axios.post(
        "https://ac.master-pos.com/api/team/account/search",
        article,
        { headers }
      )
    ).data;
  }

  async login(username, password) {
    const formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    const headers = {
      Accept: `application/json, text/plain, */*`,
    };
    return await (
      await axios.post(
        "https://ac.master-pos.com/api/team/users/authenticate",
        formData,
        { headers }
      )
    ).data;
  }
}
export default new Accounts();
