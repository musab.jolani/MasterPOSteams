import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import createPersistedState from "vuex-persistedstate";
import SecureLS from "secure-ls";
const ls = new SecureLS({ isCompression: false });

import Search from "../views/Search.vue";
Vue.use(Vuex);

const vuexPersist = new VuexPersist({
  key: "FeatureFlags",
  storage: window.localStorage,
});

const dataState = createPersistedState({
  key: "TokenMusab",
  storage: sessionStorage,
});

export default new Vuex.Store({
  state: { JWToken: "" },
  modules: {},
  mutations: {
    setToken: (state, token) => (state.JWToken = token),
  },
  plugins: [dataState],
});
