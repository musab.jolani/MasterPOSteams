import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import Search from "./views/Search.vue";
Vue.config.productionTip = false;

import axios from "axios";
//axios.defaults.baseURL = "https:///";

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
